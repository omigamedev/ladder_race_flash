package
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Elastic;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class ResultButton extends MovieClip
	{
		public function ResultButton()
		{
			super();
			addEventListener(Event.ADDED, onAdded);
			buttonMode = true;
			mouseChildren = false;
		}
		
		protected function onAdded(event:Event):void
		{
			removeEventListener(Event.ADDED, onAdded);
			addEventListener(Event.REMOVED, onRemoved);
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		}
		
		protected function onMouseOut(event:MouseEvent):void
		{
			TweenMax.to(this, 1, {scaleX:1, scaleY:1});
		}
		
		protected function onMouseOver(event:MouseEvent):void
		{
			TweenMax.to(this, 1, {scaleX:1.2, scaleY:1.2, ease:Elastic.easeOut});
		}
		
		protected function onRemoved(event:Event):void
		{
			removeEventListener(Event.REMOVED, onRemoved);
			removeEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			removeEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		}
	}
}