package
{
	import flash.display.MovieClip;
	
	public class GameIsland extends MovieClip
	{
		public var ladder1:MovieClip;
		public var ladder2:MovieClip;
		public var ladder3:MovieClip;
		public var bushL:MovieClip;
		public var bushR:MovieClip;
		
		public function GameIsland()
		{
			super();
		}
		
		public function getLadder(index:int):MovieClip
		{
			if(index == 1) return ladder1;
			if(index == 2) return ladder2;
			if(index == 3) return ladder3;
			return null;
		}
		
		public function getLadderIndex(o:MovieClip):int
		{
			if(ladder1 == o) return 1;
			if(ladder2 == o) return 2;
			if(ladder3 == o) return 3;
			return 0;
		}
	}
}