package
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.filters.BlurFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.text.TextField;
	
	public class Result extends LadderView
	{
		public var btnMap:ResultButton;
		public var btnReplay:ResultButton;
		public var btnNext:ResultButton;
		public var s1:ResultStar;
		public var s2:ResultStar;
		public var s3:ResultStar;
		public var bg:Bitmap;
		public var bgd:BitmapData;
		public var txt1:TextField;
		public var txt2:TextField;
		//public var txtCorrect:TextField;
		public var txtWrong:TextField;
		public var txtScore:TextField;
		
		public function Result(nstars:int, correct:String, wrong:String, score:int, cleared:Boolean)
		{
			super();
			x = 512;
			y = 385;
			
			s1.yellow = nstars >= 1;
			s2.yellow = nstars >= 2;
			s3.yellow = nstars >= 3;
			
			//txtCorrect.text = correct;
			txtWrong.text = wrong;
			txtScore.text = "Score: " + score.toFixed();
			
			if(!cleared)
			{
				removeChild(btnNext);
				txt1.text = txt2.text = "Try Again!";
			}
			else
			{
				txt1.text = txt2.text = "Level Cleared!";
			}
			
			addEventListener(Event.REMOVED, onRemoved);
		}
		
		override public function start():void
		{
			SoundMgr.playSweep();
			super.start();
		}
		
		protected function onRemoved(event:Event):void
		{
			removeEventListener(Event.REMOVED, onRemoved);
			if(bgd)
			{
				bgd.dispose();
				bgd = null;
				removeChild(bg);
				bg = null;
			}
		}
		
		public function setBackground(o:DisplayObject)
		{
			if(bgd == null)
			{
				bgd = new BitmapData(1024, 768, false);
				bg = new Bitmap(bgd);
				bg.x = -1024/2;
				bg.y = -768/2;
				addChildAt(bg, 0);
				//addChild(bg);
			}
			var m:Matrix = new Matrix();
			m.translate(o.x, o.y);
			bgd.draw(o, m);
			bgd.applyFilter(bgd, bgd.rect, new Point(), new BlurFilter(16, 16));
		}
	}
}