package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class SelectIsland extends MovieClip
	{
		public var cy:Number;
		public var theta:Number;
		
		public function SelectIsland()
		{
			super();
			cy = y;
			theta = 0;
			addEventListener(Event.ADDED, onAdded);
		}
		
		protected function onAdded(event:Event):void
		{
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
			addEventListener(Event.REMOVED, onRemove);
		}
		
		protected function onRemove(event:Event):void
		{
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			removeEventListener(Event.REMOVED, onRemove);
		}
		
		protected function onEnterFrame(event:Event):void
		{
			y = cy + Math.cos(theta) * 10;
			theta += 0.1;
		}
	}
}