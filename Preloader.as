package
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Linear;
	
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import flash.text.TextField;
	
	public class Preloader extends MovieClip
	{
		public var txt:TextField;
		public var ld:Loader;
		public var scroller:MovieClip;
		public var fader:Sprite;
		
		public function Preloader()
		{
			super();
			fader = new Sprite();
			fader.graphics.beginFill(0x0);
			fader.graphics.drawRect(0, 0, 1024, 768);
			fader.graphics.endFill();
			fader.cacheAsBitmap = true;
			
			cacheAsBitmap = true;
			
			scroller.stop();
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		protected function onAdded(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			txt.text = "";
			ld = new Loader();
			ld.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoaded);
			ld.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgress);
			ld.load(new URLRequest("ladder_race.swf?uncache=" + new Date().time));
		}
		
		protected function onProgress(e:ProgressEvent):void
		{
			var loaded:Number = e.bytesLoaded / e.bytesTotal; 
			txt.text = (loaded*100).toFixed(0) + "%";
			scroller.gotoAndStop(Math.ceil(loaded*100));
		}
		
		protected function onLoaded(event:Event):void
		{
			addChild(fader);
			fader.alpha = 0;
			TweenLite.to(fader, 1, {alpha:1, ease:Linear.easeIn, onCompleteParams:[this], onComplete:function(o:MovieClip){
				o.stage.addChild(ld.content);
				stage.removeChild(o);
			}});
		}
	}
}