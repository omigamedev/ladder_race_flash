package
{
	import flash.display.MovieClip;
	
	public class GamePause extends MovieClip
	{
		public var btnMap:ResultButton;
		public var btnReplay:ResultButton;
		public var btnResume:ResultButton;
		
		public function GamePause()
		{
			super();
		}
	}
}