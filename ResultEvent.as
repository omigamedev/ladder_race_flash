package
{
	import flash.events.Event;
	
	public class ResultEvent extends Event
	{
		public static const REPLAY:String = "replay";
		public static const MAP:String = "map";
		public static const NEXT:String = "next";
		
		public function ResultEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}