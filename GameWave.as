package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class GameWave extends MovieClip
	{
		public var thetaSpeed:Number;
		public var theta:Number;
		public var original_y:Number;
		public var original_x:Number;
		
		public function GameWave()
		{
			super();
			addEventListener(Event.ADDED, onAdded);
		}
		
		protected function onAdded(event:Event):void
		{
			removeEventListener(Event.ADDED, onAdded);
			initListeners();
		}
		
		private function initListeners():void
		{
			addEventListener(Event.REMOVED, onRemove);
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function removeListeners():void
		{
			removeEventListener(Event.REMOVED, onRemove);
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		protected function onRemove(event:Event):void
		{
			removeListeners();
		}
		
		protected function onEnterFrame(event:Event):void
		{
			x = original_x + Math.cos(theta) * 50.0;
			y = original_y + Math.sin(theta*0.3) * 5.0;
			theta += thetaSpeed;
		}
	}
}