package
{
	import flash.display.MovieClip;
	
	public class GameCoin extends MovieClip
	{
		public var line:MovieClip;
		private var _yellow:Boolean;
		private var _wrong:Boolean;
		
		public function GameCoin()
		{
			super();
			yellow = false;
			wrong = false;
			scaleX = scaleY = 0.45;
		}
		
		public function get wrong():Boolean
		{
			return _wrong;
		}

		public function set wrong(value:Boolean):void
		{
			_wrong = value;
			line.visible = value;
		}

		public function get yellow():Boolean
		{
			return _yellow;
		}
		
		public function set yellow(value:Boolean):void
		{
			_yellow = value;
			gotoAndStop(value ? "yellow" : "grey");
		}

	}
}