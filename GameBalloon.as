package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class GameBalloon extends MovieClip
	{
		public var color_sprite:MovieClip;
		
		public var theta:Number;
		public var velx:Number;
		public var cx:Number;
		private var _color:uint;
		
		public function GameBalloon()
		{
			super();
			theta = 0;
			velx = (Math.random()*2-1)*2;
		}
		
		public function get color():uint
		{
			return _color;
		}

		public function set color(value:uint):void
		{
			_color = value;
			
			color_sprite["rect"].graphics.beginFill(value);
			color_sprite["rect"].graphics.drawRect(-100, -100, 200, 200);
			color_sprite["rect"].graphics.endFill();
		}

		public function fly():void
		{
			cx = x;
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		protected function onEnterFrame(event:Event):void
		{
			y -= 10;
			rotation = Math.sin(theta)*20;
			x += velx;
			theta += velx*0.08;
			if(y < -600)
			{
				removeEventListener(Event.ENTER_FRAME, onEnterFrame);
				parent.removeChild(this);
			}
		}
	}
}