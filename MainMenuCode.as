package
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	
	public class MainMenuCode extends MovieClip
	{
		public var txt:TextField;
		public var txtError:TextField;
		public var btn:MovieClip;
		
		public function MainMenuCode()
		{
			super();
			btn.buttonMode = true;
			btn.mouseChildren = false;
			txt.text = "";
			txtError.text = "";
		}
	}
}