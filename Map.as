package
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	public class Map extends LadderView
	{
		public static const STARTER:int = 0;
		public static const MOVER:int = 1;
		public static const FLYER:int = 2;
		
		public var lv1:MapLevel;
		public var lv2:MapLevel;
		public var lv3:MapLevel;
		public var lv4:MapLevel;
		public var txtScore:TextField;
		public var btnBack:MovieClip;
		
		private var layouts:Array;
		
		public function Map(pgType:String, score:int, level:int, stars:Array, layout:int)
		{
			super();
			x = 512;
			y = 393;
			
			layouts = new Array();
			layouts[STARTER] = [
				{x:-253, y:54},
				{x:-110, y:-127},
				{x:117, y:-1},
				{x:235, y:-157},
			];
			layouts[MOVER] = [
				{x:-253, y:54},
				{x:-128, y:-90},
				{x:86, y:-55},
				{x:290, y:78},
				{x:235, y:-157},
			];
			layouts[FLYER] = [
				{x:-253, y:54},
				{x:-128, y:-90},
				{x:2, y:-10},
				{x:152, y:32},
				{x:287, y:40},
				{x:235, y:-157},
			];
			
			for(var i:int = 0; i < 6; i++)
			{
				var lv:MapLevel = this["lv" + (i+1)] as MapLevel;
				if(i < layouts[layout].length)
				{
					if(i <= level)
					{
						lv.level = i+1;
						lv.locked = false;
						lv.stars = stars[i];
					}
					lv.x = layouts[layout][i].x;
					lv.y = layouts[layout][i].y;
				}
				else
				{
					removeChild(lv);
				}
			}
			
			txtScore.text = "Score: " + score.toFixed();
			tabChildren = false;
			btnBack.buttonMode = true;
			
			var pg:CharacterBase;
			if(pgType == CharacterBase.PG_CAT) pg = new CatFront();
			else if(pgType == CharacterBase.PG_HAMSTER) pg = new HamsterFront();
			pg.scaleX = -0.4;
			pg.scaleY = 0.4;
			pg.x = -390;
			pg.y = 310;
			addChild(pg);
		}
	}
}