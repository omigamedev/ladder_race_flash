package
{
	import com.demonsters.debugger.MonsterDebugger;
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Bounce;
	import com.greensock.easing.Elastic;
	import com.greensock.easing.Linear;
	
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.ui.Keyboard;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	
	public class Game extends LadderView
	{
		public static const RES_SUCCESS:String = "success"; 
		public static const RES_FAILURE:String = "failure";
		
		public var blackboard:GameBlackboard;
		public var score:GameScore;
		public var island:GameIsland;
		public var cloudBigL:MovieClip;
		public var cloudBigR:MovieClip;
		public var cloudBigR2:MovieClip;
		public var cloudMidL:MovieClip;
		public var cloudMidR:MovieClip;
		public var cloudSmallL:MovieClip;
		public var cloudSmallR:MovieClip;
		public var txt_timer:TextField;
		public var txtLevel:TextField;
		public var btnPause:MovieClip;
		public var btnMute:GameButtonMute;
		public var btnFS:MovieClip;
		public var pictureFrame:MovieClip;
		public var mpause:GamePause;
		
		private var pg:CharacterBase;
		private var pg_front:CharacterBase;
		private var pg_back:CharacterBase;
		private var currentLadder:MovieClip;
		private var coins:Vector.<GameCoin>;
		private var movingCoin:GameCoin;
		private var imgLoader:Loader;
		private var fallingWords:Vector.<GameFallingWord>;
		private var balloons:Vector.<GameBalloon>;
		
		private var pictureFrame_ox:Number;
		
		private var keys:Vector.<Boolean>;
		private var island_y:Number;
		private var island_theta:Number;
		private var pg_ybase:Number;
		private var keylocked:Boolean;
		private var points_to:int;
		private var last_result:String;
		private var timerInterval:uint;
		private var timerCount:int;
		private var gameEnded:Boolean;
		private var gameEnding:Boolean;
		private var pgMoving:Boolean;
		private var pgWasPlaying:Boolean;
		private var firstTimer:uint;
		
		private var level:int;
		private var ncoins:int;
		private var ncorrect:int;
		private var wordsList:Array;
		private var wordsIndex:int;
		private var wordsRetry:Boolean;
		private var wordsVel:Number;
		private var listCorrect:String;
		private var listWrong:String;
		
		public function Game(pg_type:String, wl:Array, lvl:int)
		{
			trace("total words: " + wl.length);
			
			var i:int;
			var j:int;
			
			super();
			wordsList = wl;
			score.txt.text = "0";
			x = 512;
			y = 505;
			
			if(pg_type == CharacterBase.PG_CAT)
			{
				pg_back = new CatBack();
				pg_front = new CatFront();
			}
			else if(pg_type == CharacterBase.PG_HAMSTER)
			{
				pg_back = new HamsterBack();
				pg_front = new HamsterFront();
			}
			MonsterDebugger.inspect(pg_front);
			MonsterDebugger.inspect(pg_back);
			pg = pg_front;
			pg.scaleX = pg.scaleY = 0.0;
			pg.y = 80;
			pg_ybase = island.ladder1.y;
			island.addChild(pg);
			island_y = island.y;
			island_theta = 0;
			currentLadder = null;
			score.rotation = 150;
			blackboard.y += 150;
			btnMute.y += 150;
			btnPause.y += 150;
			btnFS.y += 150;
			txt_timer.alpha = 0;
			txtLevel.alpha = 0;
			keylocked = false;
			ncoins = 0;
			ncorrect = 0;
			gameEnded = false;
			pgMoving = false;
			tabChildren = false;
			listCorrect = "";
			listWrong = "";
			
			level = lvl;
			txtLevel.text = "Level " + (level + 1);
			
			//trace("playing level " + level);
			//if(level <= 2) wordsVel = 1;
			//if(level == 3) wordsVel = 1.5;
			//if(level == 4) wordsVel = 2;
			//if(level == 5) wordsVel = 3;
			
			if(LadderRace.configGroup == Map.STARTER) wordsVel = 1.0 + level * 0.05;
			if(LadderRace.configGroup == Map.MOVER) wordsVel = 1.0 + level * 0.1;
			if(LadderRace.configGroup == Map.FLYER) wordsVel = 1.0 + level * 0.2;
			trace("wordsVel=" + wordsVel);
			
			pictureFrame.visible = false;
			pictureFrame_ox = pictureFrame.x;
			
			mpause = new GamePause();
			mpause.btnResume.addEventListener(MouseEvent.CLICK, function(e:Event)
			{
				mpause.mouseEnabled = false;
				TweenMax.to(mpause, 0.5, {alpha:0, onComplete:function(){
					mouseEnabled = true;
					gameResume();
					removeChild(mpause);
				}});
			});
			
			btnFS.buttonMode = true;
			btnFS.addEventListener(MouseEvent.CLICK, function(e:Event)
			{
				if(stage.allowsFullScreen == false) trace("fullscreen not allowed");
				if(stage.allowsFullScreenInteractive == false) trace("fullscreen-interactive not allowed");
				if(stage.displayState == StageDisplayState.FULL_SCREEN)
					stage.displayState = StageDisplayState.NORMAL;
				else
					stage.displayState = StageDisplayState.FULL_SCREEN;
			});
			
			btnPause.buttonMode = true;
			btnPause.mouseChildren = false;
			btnPause.mouseEnabled = false;
			btnPause.addEventListener(MouseEvent.CLICK, function(e:Event)
			{
				SoundMgr.playSweep();
				gamePause();
				mpause.alpha = 0;
				mpause.y = -120;
				mouseEnabled = false;
				mpause.mouseEnabled = true;
				addChild(mpause);
				TweenMax.to(mpause, 0.5, {alpha:1, onComplete:function(){
					
				}});
			});
			
			btnMute.muted = SoundMgr.muted;
			
			keys = new Vector.<Boolean>(256, true);
			fallingWords = new Vector.<GameFallingWord>();
			balloons = new Vector.<GameBalloon>();
			coins = new Vector.<GameCoin>();
			
			wordsIndex = 0;
			
			var y1:Number = -450;
			var y2:Number = 0;
			var n:Number = 6;
			for(i = 0; i < n; i++)
			{
				var star:GameCoin = new GameCoin();
				star.x = -550;
				star.y = y1 + (y2-y1)/(n-1) * i;
				addChild(star);
				coins.push(star);
			}
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		private function gamePause()
		{
			pgWasPlaying = pg.isPlaying;
			removeListeners();
			timerStop();
			TweenMax.pauseAll();
			for(var i:int = 0; i < fallingWords.length; i++)
			{
				fallingWords[i].removeListeners();
			}
		}
		
		private function gameResume()
		{
			if(pgWasPlaying) pg.play();
			resetKeys();
			initListeners();
			timerStart(true);
			TweenMax.resumeAll();
			for(var i:int = 0; i < fallingWords.length; i++)
			{
				fallingWords[i].initListeners();
			}
		}
		
		private function initListeners():void
		{	
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		protected function onAdded(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		override public function start():void
		{
			super.start();
			TweenMax.to(score, 1, {delay:1.5, rotation:0, ease:Bounce.easeOut});
			TweenMax.to(blackboard, 1.5, {delay:1.5, y:250, ease:Elastic.easeOut});
			TweenMax.to(btnMute, 1.5, {delay:1.5, y:213, ease:Elastic.easeOut});
			TweenMax.to(btnPause, 1.5, {delay:1.5, y:213, ease:Elastic.easeOut});
			TweenMax.to(btnFS, 1.5, {delay:1.5, y:213, ease:Elastic.easeOut});
			TweenMax.to(txt_timer, 1.5, {delay:1.5, alpha:1});
			TweenMax.to(txtLevel, 1.5, {delay:1.5, alpha:1});
			TweenMax.to(pg, 1.0, {delay:0.5, scaleX:0.4, scaleY:0.4, ease:Elastic.easeOut});
			
			for(var i:int = 0; i < coins.length; i++)
			{
				coins[i].x = -550;
				TweenMax.to(coins[i], 2.0, {delay:0.3 * i + 1, x:-450, ease:Elastic.easeOut});
			}
			
			TweenMax.delayedCall(3, tweenComplete);
			SoundMgr.playBG();
		}
		
		private function timerStart(resume:Boolean = false)
		{
			if(!resume) timerCount = 95;
			timerInterval = setInterval(timerTick, 1000);
			trace("timer " + timerInterval);
		}
		
		private function timerTick()
		{
			timerCount--;
			txt_timer.text = timerCount.toFixed();
			SoundMgr.playTick();
			if(timerCount == 0)
			{
				if(keylocked) gameEnded = true;
				else pgDown(ncorrect == 6 ? CharacterBase.ANIM_HAPPY : CharacterBase.ANIM_SAD, null, gameEnd);
				gameEnding = true;
				timerStop();
				// reset words
				for(var i:int = 0; i < fallingWords.length; i++)
				{
					importBalloon(fallingWords[i]);
					removeChild(fallingWords[i]);
				}
				fallingWords = new Vector.<GameFallingWord>();
			}
		}
		
		private function gameEnd(e:Event = null)
		{
			removeListeners();
			pg.reset();
			pg.direction(pg.x < 0 ? CharacterBase.DIR_R : CharacterBase.DIR_L);
			pg.walk();
			TweenMax.to(pg, 1, {x:0, y:pg_ybase, ease:Linear.easeNone,
				onUpdate:pg.walk,
				onComplete:function()
				{
					switchBack();
					pg.climb();
					TweenMax.to(pg, 5, {y:-600, ease:Linear.easeNone, 
						//onComplete:gameEnd_done,
						onUpdate:pg.climb
					});
				}
			});
			
			// wait for the animation to be completed
			TweenMax.delayedCall(2, gameEnd_done);
			
			// play win o lose sound
			if(levelPassed()) SoundMgr.playWin();
			else SoundMgr.playLose();
		}
		
		private function gameEnd_done()
		{
			var ge:GameEvent = new GameEvent(GameEvent.COMPLETE);
			ge.nTotal = 6;
			ge.nRight = ncorrect;
			ge.nWrong = 6-ncorrect;
			ge.ncoins = ncorrect;
			ge.listCorrect = listCorrect;
			ge.listWrong = listWrong;
			ge.score = points_to;
			ge.levelPassed = levelPassed();
			
			var swords:String = "";
			for(var i:int = 0; i < 6; i++)
			{
				if(swords.length > 0) swords += ",";
				swords += wordsList[i].en;
			}
			
			ge.wordsListString = swords;
			
			dispatchEvent(ge);
		}
		
		private function levelPassed():Boolean
		{
			if(level <= 2) return ncorrect >= 4;
			if(level == 3) return ncorrect >= 5;
			if(level == 4) return ncorrect >= 6;
			if(level == 5) return ncorrect >= 6;
			return false;
		}
		
		private function timerStop()
		{
			clearInterval(timerInterval);
			timerInterval = 0;
		}
		
		private function tweenComplete():void
		{
			timerStart();
			initListeners();
			TweenMax.killTweensOf(pg);
			wordsAdd_interval();
			btnPause.mouseEnabled = true;
		}
		
		private function wordsAdd_interval(e:Event = null):void
		{
			if(gameEnded || gameEnding) return;
			
			var cur:Object = wordsList[wordsIndex];
			var line:Array = [cur.en, cur.wr1, cur.wr2];
			line.sort(function(a:*, b:*){return Math.random() < 0.5 ? 1 : -1;});
			blackboard.txt.text = cur.tr;
			
			wordsAdd(line[0], 1, 0);
			wordsAdd(line[1], 2, 100);
			wordsAdd(line[2], 3, 50);
			
			// load word thumb
			var simg:String = (cur.en as String).replace("to ", "");
			var r:URLRequest = new URLRequest("small/" + simg + ".gif");
			//var r:URLRequest = new URLRequest("images/sausage.gif");
			if(imgLoader != null)
			{
				if(imgLoader.content && contains(imgLoader.content)) 
					removeChild(imgLoader.content);
				imgLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, imgLoaded);
			}
			imgLoader = new Loader();
			imgLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, imgLoaded);
			imgLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, imgError);
			imgLoader.load(r);
			pictureFrame.visible = false;
			trace("new word " + r.url);
		}
		
		protected function imgError(event:IOErrorEvent):void
		{
			trace("error opening image");
		}
		
		private function imgLoaded(event:Event):void
		{
			var b:Bitmap = imgLoader.content as Bitmap;
			var ratio:Number = b.width / b.height;
			trace(ratio);
			if(ratio > 1)
			{
				b.width = 150;
				b.height = b.width / ratio;
			}
			else
			{
				b.height = 150;
				b.width = b.height * ratio;
			}
			pictureFrame.width = b.width + 75;
			pictureFrame.height = b.height + 70;
			b.smoothing = true;
			b.opaqueBackground = 0xFFFFFF;
			pictureFrame.visible = true;
			addChildAt(b, getChildIndex(pictureFrame));
			
			pictureFrame.x = 500;
			var tox:Number = pictureFrame_ox+(200-pictureFrame.width)*0.5;
			TweenMax.to(pictureFrame, 1, {x:tox, ease:Elastic.easeOut, onUpdateParams:[b],
				onUpdate:function(b:Bitmap)
				{
					b.x = pictureFrame.x + 40;
					b.y = pictureFrame.y + 37;
				}});
			b.x = pictureFrame.x + 40;
			b.y = pictureFrame.y + 37;
		}
		
		private function arrayContains(ar:Array, o:String):Boolean
		{
			for(var i:int = 0; i < ar.length; i++)
				if(ar[i] == o) return true;
			return false;
		}
		
		private function wordsAdd(s:String, islandIndex:int, delay:Number):void
		{
			var fw:GameFallingWord = new GameFallingWord(wordsVel);
			fw.y = -550-delay;
			fw.x = island.getLadder(islandIndex).x;
			fw.text = s;
			fallingWords.push(fw);
			addChild(fw);
		}
		
		private function removeListeners():void
		{
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.removeEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			timerStop();
		}
		
		protected function onEnterFrame(event:Event):void
		{
			var velx:Number = 0;
			var vely:Number = 0;
			
			if(!pgMoving && gameEnded) gameEnd(null);
			
			// update score
			if(score.value < points_to)
			{
				score.value += 2;
			}
			
			// float the island
			island.y = island_y + Math.sin(island_theta) * 10;
			island_theta += 0.03;
			
			// word missed
			if(fallingWords.length > 0 && fallingWords[0].y > 120)
			{
				var oldwords:Array = [fallingWords[0].text, fallingWords[1].text, fallingWords[2].text];
				
				// reset words
				for(var i:int = 0; i < fallingWords.length; i++)
				{
					importBalloon(fallingWords[i]);
					removeChild(fallingWords[i]);
				}
				fallingWords = new Vector.<GameFallingWord>();
				
				if(wordsRetry == false)
				{
					wordsAdd(oldwords[0], 1, 0);
					wordsAdd(oldwords[1], 2, 100);
					wordsAdd(oldwords[2], 3, 50);
					
					wordsRetry = true;
				}
				else
				{
					var destCoin:GameCoin = coins[coins.length-1-ncoins];
					destCoin.wrong = true;
					wordsIndex++;
					wordsRetry = false;
					wordsAdd_interval();
				}
				pg.sad();
				return;
			}
			
			if(!(keylocked || pg.locked))
			{
				if(keys[Keyboard.LEFT]) velx -= 5;
				if(keys[Keyboard.RIGHT]) velx += 5;
				if(keys[Keyboard.UP]) vely -= 8;
				if(keys[Keyboard.DOWN]) vely += 8;
			}
			
			// find current ladder near to the pg
			currentLadder = null;
			if(Math.abs(island.ladder1.x - pg.x) < 30) currentLadder = island.ladder1; 
			if(Math.abs(island.ladder2.x - pg.x) < 30) currentLadder = island.ladder2; 
			if(Math.abs(island.ladder3.x - pg.x) < 30) currentLadder = island.ladder3;
			
			if(pg.y <= pg_ybase && currentLadder != null)
			{
				// switch to pg_back
				switchBack();
				
				// align the pg with the ladder
				pg.x = currentLadder.x;
				
				if(vely < 0)
				{
					pg.climb();
					pg.y = clamp(pg.y + vely, -350, pg_ybase+50);
				}
				else if(vely > 0) 
				{
//					if(pg.y < pg_ybase || pg.animating) pg.down();
//					else pg.y = clamp(pg.y + vely, -300, pg_ybase+50);
					pg.climb();
					pg.y = clamp(pg.y + vely, -350, pg_ybase+50);
				}
				else if(velx > 0)
				{
					if(currentLadder != island.ladder3) pg.jump();
					else if(pg.y < -80) pg.fear();
					else pg.stand();
				}
				else if(velx < 0)
				{
					if(currentLadder != island.ladder1) pg.jump();
					else if(pg.y < -80) pg.fear();
					else pg.stand();
				}
				else pg.stand();
			}
			else if(pg.y >= pg_ybase)
			{
				// switch to pg_front
				switchFront();
				
				pg.x = clamp(pg.x + velx, -250, 270);
				pg.y = clamp(pg.y + vely, pg_ybase, pg_ybase+50);
				
				if(velx > 0)
				{
					if(pg.x == 270) pg.fear();
					else pg.walk();
				}
				else if(velx < 0)
				{
					if(pg.x == -250) pg.fear();
					else pg.walk();
				}
				else if(vely != 0)
				{
					pg.walk();
				}
				else
				{
					pg.stand();
				}
			}
			
			// pg orientation
			if(velx > 0) pg.direction(CharacterBase.DIR_R);
			if(velx < 0) pg.direction(CharacterBase.DIR_L);
			
			// move pg in foreground or back the bushes
			if(pg.y < 60) island.setChildIndex(pg, island.numChildren - 4);
			else island.setChildIndex(pg, island.numChildren - 2);
			
			// move clouds
			cloudBigL.x += 0.4;
			if(cloudBigL.x > 700) cloudBigL.x = -700;
			cloudBigR.x -= 0.8;
			if(cloudBigR.x < -700) cloudBigR.x = 700;
			cloudBigR2.x -= 0.2;
			if(cloudBigR2.x < -700) cloudBigR2.x = 700;
			
			// check words collision
			checkCollisions();
		}
		
		private function checkCollisions():void
		{
			if(!(currentLadder && fallingWords.length > 2)) return;
			var w:GameFallingWord = fallingWords[island.getLadderIndex(currentLadder) - 1];
			var wp:Point = new Point(w.x, w.y+30);
			wp = localToGlobal(wp);
			
			// if no collision exit
			if(!pg.hitTestPoint(wp.x, wp.y, true)) return;
			
			trace("HIT");
			var hit:MovieClip;
			var cur:Object = wordsList[wordsIndex];
			var destCoin:GameCoin = coins[coins.length-1-ncoins];
			if(w.text == cur.en)
			{
				hit = new GameHitStars()
				points_to += 100;
				last_result = RES_SUCCESS;
				SoundMgr.playStars();
				
				wordsIndex++;
				if(wordsIndex == 6)
				{
					gameEnded = true;
				}
				
				listCorrect += cur.tr + " = " + cur.en + "\n";
				trace(cur.en + " = " + cur.tr);
				
				// get the coin
				if(movingCoin == null) movingCoin = new GameCoin();
				movingCoin.x = w.x;
				movingCoin.y = w.y;
				movingCoin.yellow = true;
				addChild(movingCoin);
				TweenMax.to(movingCoin, 0.5, {x:destCoin.x, y:destCoin.y, 
					onCompleteParams:[this, destCoin], ease:Linear.easeNone,
					onComplete:function(t:MovieClip, d:GameCoin)
					{
						t.removeChild(movingCoin);
						d.yellow = true;
						ncorrect++;
					}
				});
			}
			else
			{
				hit = new GameHitSmoke();
				last_result = RES_FAILURE;
				SoundMgr.playPunch();
				wordsIndex++;
				destCoin.wrong = true;
				
				listWrong += cur.tr + " = " + cur.en + "\n";
				trace(cur.en + " = " + cur.tr);
				
				if(wordsIndex == 6)
				{
					gameEnded = true;
				}
			}
			
			ncoins++;
			
			hit.x = w.x;
			hit.y = w.y;
			addChild(hit);
			
			for(var i:int = 0; i < fallingWords.length; i++)
			{
				importBalloon(fallingWords[i]);
				removeChild(fallingWords[i]);
			}
			fallingWords = new Vector.<GameFallingWord>();
			if(last_result == RES_SUCCESS)
				pgDown(CharacterBase.ANIM_HAPPY, null, wordsAdd_interval);
			if(last_result == RES_FAILURE)
				pgDown(CharacterBase.ANIM_SAD, null, wordsAdd_interval);
		}
		
		private function pgDown(animType:String, callbackDown:Function, callbackAnimated:Function)
		{
			if(pg.y <= pg_ybase)
			{
				keylocked = true;
				pgMoving = true;
				resetKeys();
				//var delay:Number = pg.animating ? 1000 : 0;
				//setTimeout(function(){
				TweenMax.to(pg, 1, {overwrite:3, x:0, ease:Linear.easeIn});
				TweenMax.to(pg, 0.7, {overwrite:0, y:pg_ybase+55, ease:Back.easeIn, 
					onComplete:function()
					{
						switchFront();
						if(animType == CharacterBase.ANIM_HAPPY)
						{
							pg.happy();
							if(callbackAnimated != null)
							{
								pg.addEventListener(CharacterBase.EVENT_HAPPY, pgMoveComplete);
								pg.addEventListener(CharacterBase.EVENT_HAPPY, callbackAnimated);
							}
						}
						else if(animType == CharacterBase.ANIM_SAD)
						{
							pg.sad();
							if(callbackAnimated != null)
							{
								pg.addEventListener(CharacterBase.EVENT_SAD, pgMoveComplete);
								pg.addEventListener(CharacterBase.EVENT_SAD, callbackAnimated);
							}
						}
						pg.locked = true;
						keylocked = false;
						if(callbackDown != null) callbackDown();
					}
				});
				//}, delay);
			}
			else
			{
				pg.locked = true;
				keylocked = true;
				switchFront();
				resetKeys();
				pgMoving = true;
				if(animType == CharacterBase.ANIM_HAPPY)
				{
					pg.reset();
					pg.happy();
					if(callbackAnimated != null)
					{
						pg.addEventListener(CharacterBase.EVENT_HAPPY, pgMoveComplete);
						pg.addEventListener(CharacterBase.EVENT_HAPPY, callbackAnimated);
					}
				}
				else if(animType == CharacterBase.ANIM_SAD)
				{
					pg.reset();
					pg.sad();
					if(callbackAnimated != null)
					{
						pg.addEventListener(CharacterBase.EVENT_SAD, pgMoveComplete);
						pg.addEventListener(CharacterBase.EVENT_SAD, callbackAnimated);
					}
				}
				if(callbackDown != null) callbackDown();
			}
		}
		
		private function pgMoveComplete(e:Event)
		{
			pgMoving = false;
			pg.locked = false;
			keylocked = false;
		}
		
		private function switchBack():void
		{
			if(pg != pg_back && island.contains(pg))
			{
				island.removeChild(pg);
				pg.reset();
				pg_back.x = pg_front.x;
				pg_back.y = pg_front.y;
				pg_back.scaleX = pg_front.scaleX;
				pg_back.scaleY = pg_front.scaleY;
				pg = pg_back;
				island.addChild(pg);
			}
		}
		
		private function switchFront():void
		{
			if(pg != pg_front && island.contains(pg))
			{
				island.removeChild(pg);
				pg.reset();
				pg_front.x = pg_back.x;
				pg_front.y = pg_back.y;
				pg_front.scaleX = pg_back.scaleX;
				pg_front.scaleY = pg_back.scaleY;
				pg = pg_front;
				pg.stand();
				island.addChild(pg);
			}
		}
		
		private function importBalloon(w:GameFallingWord):void
		{
			var b:GameBalloon = w.balloon;
			var p:Point = new Point(b.x, b.y);
			p = w.localToGlobal(p);
			p = globalToLocal(p);
			b.x = p.x;
			b.y = p.y;
			w.removeChild(b);
			addChild(b);
			b.fly();
		}
		
		private function resetKeys():void
		{
			for(var i:int = 0; i < keys.length; i++)
			{
				keys[i] = false;
			}
		}
		
		protected function onKeyUp(event:KeyboardEvent):void
		{
			keys[event.keyCode] = false;
		}
		
		protected function onKeyDown(event:KeyboardEvent):void
		{
			keys[event.keyCode] = true;
		}
		
		private function clamp(n:Number, a:Number, b:Number):Number
		{
			if(n < a) return a;
			if(n > b) return b;
			return n;
		}
		
	}
}