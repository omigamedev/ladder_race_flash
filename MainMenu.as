package
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;

	public class MainMenu extends LadderView
	{
		public var btnPlay:MovieClip;
		public var btnRegister:MovieClip;
		public var btnEdit:MovieClip;
		public var code:MainMenuCode;
		
		public function MainMenu(registered:Boolean)
		{
			super();
			x = 512;
			y = 384;
			
			btnPlay.buttonMode = true;
			btnEdit.buttonMode = true;
			btnRegister.buttonMode = true;
			tabChildren = false;
			
			if(!registered)
			{
				btnRegister.y = 210;
				code.y = 325;
				removeChild(btnPlay);
				removeChild(btnEdit);
			}
			else
			{
				btnPlay.y = 215;
				//btnEdit.y = 300;
				removeChild(btnRegister);
				removeChild(code);
				removeChild(btnEdit);
			}
		}
	}
}