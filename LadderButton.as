package
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	public class LadderButton extends MovieClip
	{
		public var bg:MovieClip;
		
		public function LadderButton()
		{
			super();
			buttonMode = true;
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			onMouseOut(null);
			mouseChildren = false;
		}
		
		protected function onMouseOut(event:MouseEvent):void
		{
			bg.alpha = 0.7;
		}
		
		protected function onMouseOver(event:MouseEvent):void
		{
			bg.alpha = 1.0;
		}
	}
}