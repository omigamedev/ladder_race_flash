package
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.media.SoundMixer;
	import flash.text.TextField;
	
	public class GameButtonMute extends MovieClip
	{
		public var bg:MovieClip;
		public var txt:TextField;
		private var _muted:Boolean;
		
		public function GameButtonMute()
		{
			super();
			txt.text = "Mute";
			buttonMode = true;
			mouseChildren = false;
			addEventListener(MouseEvent.CLICK, onClick);
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			onMouseOut(null);
			mouseChildren = false;
		}
		
		protected function onMouseOut(event:MouseEvent):void
		{
			bg.alpha = 0.7;
		}
		
		protected function onMouseOver(event:MouseEvent):void
		{
			bg.alpha = 1.0;
		}
		
		public function get muted():Boolean
		{
			return _muted;
		}

		public function set muted(value:Boolean):void
		{
			_muted = value;
			txt.text = _muted ? "Unmute" : "Mute";
			SoundMgr.muted = _muted;
		}

		protected function onClick(event:MouseEvent):void
		{
			muted = !muted;
		}
	}
}