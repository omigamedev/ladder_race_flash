package
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	
	public class MapLevelInfo extends MovieClip
	{
		public var txt1:TextField;
		public var txt2:TextField;
		public var star1:MapBooleanStar;
		public var star2:MapBooleanStar;
		public var star3:MapBooleanStar;
		
		public function MapLevelInfo()
		{
			super();
		}
	}
}