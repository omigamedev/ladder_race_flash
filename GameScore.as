package
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	
	public class GameScore extends MovieClip
	{
		public var txt:TextField;
		private var _value:int;
		
		public function GameScore()
		{
			super();
		}

		public function get value():int
		{
			return _value;
		}

		public function set value(value:int):void
		{
			_value = value;
			txt.text = value.toFixed();
		}

	}
}