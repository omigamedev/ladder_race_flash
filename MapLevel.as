package
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Elastic;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class MapLevel extends MovieClip
	{
		public var island:MapLevelFloat;
		private var _level:int;
		private var _stars:int;
		private var _locked:Boolean;
		private var theta:Number;
		private var cy:Number;
		
		public function MapLevel()
		{
			super();
			theta = Math.random()*Math.PI*2;
			cy = island.y;
			stars = 0;
			level = 0;
			locked = true;
			stop();
			addEventListener(Event.ADDED, onAdded);
			buttonMode = true;
			mouseChildren = false;
		}
		
		public function get locked():Boolean
		{
			return _locked;
		}

		public function set locked(value:Boolean):void
		{
			_locked = value;
			island.info.visible = !value;
			island.lock.visible = value;
		}

		public function get stars():int
		{
			return _stars;
		}

		public function set stars(value:int):void
		{
			_stars = value;
			island.info.star1.yellow = stars >= 1;
			island.info.star2.yellow = stars >= 2;
			island.info.star3.yellow = stars >= 3;
		}

		public function get level():int
		{
			return _level;
		}

		public function set level(value:int):void
		{
			_level = value;
			island.info.txt1.text = island.info.txt2.text = value.toFixed();
		}

		protected function onAdded(event:Event):void
		{
			removeEventListener(Event.ADDED, onAdded);
			addEventListener(Event.REMOVED, onRemoved);
			addEventListener(Event.ENTER_FRAME, onFrame);
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		}
		
		protected function onMouseOut(event:MouseEvent):void
		{
			TweenMax.to(island, 1, {scaleX:1, scaleY:1});
		}
		
		protected function onMouseOver(event:MouseEvent):void
		{
			TweenMax.to(island, 1, {scaleX:1.2, scaleY:1.2, ease:Elastic.easeOut});
		}
		
		protected function onRemoved(event:Event):void
		{
			removeEventListener(Event.REMOVED, onRemoved);
			removeEventListener(Event.ENTER_FRAME, onFrame);
			removeEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			removeEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		}
		
		protected function onFrame(event:Event):void
		{
			island.y = cy + Math.cos(theta) * 5;
			theta += 0.03;
		}
	}
}