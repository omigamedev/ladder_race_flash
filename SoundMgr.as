package
{
	import com.greensock.TweenMax;
	
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;

	public class SoundMgr
	{
		public static var _muted:Boolean;
		public static var _S:SoundMgr;
		
		public var _tick:Sound;
		public var _stars:Sound;
		public var _yuppie:Sound;
		public var _feet:Sound;
		public var _fail:Sound;
		public var _punch:Sound;
		public var _sweep:Sound;
		public var _win:Sound;
		public var _lose:Sound;
		public var _bg:Sound;
		public var _buffer:Array;
		public var _buffer_snd:Array;
		
		public function SoundMgr()
		{
			
		}
		
		public static function playWin()
		{
			S.sndPlay(S.win);
		}
		
		public static function playLose()
		{
			S.sndPlay(S.lose);
		}
		
		public static function playSweep()
		{
			S.sndPlay(S.sweep);
		}
		
		public static function playFail()
		{
			S.sndPlay(S.fail);
		}
		
		public static function playPunch()
		{
			S.sndPlay(S.punch);
		}
		
		public static function playBG()
		{
			S.sndPlay(S.bg, int.MAX_VALUE);
		}
		
		public static function playTick()
		{
			S.sndPlay(S.tick);
		}
		
		public static function playFeet()
		{
			S.sndPlay(S.feet);
		}
		
		public static function stopFeet()
		{
			S.sndStop(S.feet);
		}
		
		public static function playStars()
		{
			S.sndPlay(S.stars);
		}
		
		public static function playYuppie()
		{
			S.sndPlay(S.yuppie);
		}
		
		public static function get muted():Boolean
		{
			return _muted;
		}
		
		public static function set muted(value:Boolean):void
		{
			if(_muted != value)
			{
				_muted = value;
				if(value == true) muteAll();
				else unmuteAll();
			}
		}
		
		public static function get S():SoundMgr
		{
			if(_S == null) _S = new SoundMgr();
			return _S;
		}
		
		public function get win():Sound
		{
			if(_win == null) _win = new SndWin();
			return _win;
		}
		
		public function get lose():Sound
		{
			if(_lose == null) _lose = new SndLose();
			return _lose;
		}
		
		public function get sweep():Sound
		{
			if(_sweep == null) _sweep = new SndSweep();
			return _sweep;
		}
		
		public function get fail():Sound
		{
			if(_fail == null) _fail = new SndFail();
			return _fail;
		}
		
		public function get punch():Sound
		{
			if(_punch == null) _punch = new SndPunch();
			return _punch;
		}
		
		public function get feet():Sound
		{
			if(_feet == null) _feet = new SndFeet();
			return _feet;
		}
		
		public function get tick():Sound
		{
			if(_tick == null) _tick = new SndTick();
			return _tick;
		}
		
		public function get stars():Sound
		{
			if(_stars == null) _stars = new SndStars();
			return _stars;
		}
		
		public function get yuppie():Sound
		{
			if(_yuppie == null) _yuppie = new SndYuppie();
			return _yuppie;
		}
		
		public function get bg():Sound
		{
			if(_bg == null) _bg = new SndBG();
			return _bg;
		}
		
		public function get buffer():Array
		{
			if(_buffer == null)
			{
				_buffer = new Array();
				_buffer_snd = new Array();
			}
			return _buffer;
		}
		
		public function get buffer_snd():Array
		{
			if(_buffer_snd == null)
			{
				_buffer = new Array();
				_buffer_snd = new Array();
			}
			return _buffer_snd;
		}
		
		public static function stopAll()
		{
			S.reset();
		}
		
		public static function muteAll()
		{
			S.mute();
		}
		
		public static function unmuteAll()
		{
			S.unmute();
		}
		
		public static function fadeOut(t:Number)
		{
			if(muted)
			{
				stopAll();
			}
			else
			{
				var st:SoundTransform = new SoundTransform();
				TweenMax.to(st, t, {volume:0, onUpdateParams:[st], onComplete:stopAll,
					onUpdate:function(st:SoundTransform)
				{
					var b:Array = S.buffer;
					for(var i:int = 0; i < b.length; i++)
					{
						b[i].soundTransform = st;
					}
				}});
			}
		}
		
		public function reset()
		{
			trace("soundmgr reset");
			var b:Array = buffer;
			for(var i:int = 0; i < b.length; i++)
			{
				b[i].stop();
			}
			_buffer = new Array();
			_buffer_snd = new Array();
		}
		
		public function mute()
		{
			trace("soundmgr mute");
			var b:Array = buffer;
			for(var i:int = 0; i < b.length; i++)
			{
				var st:SoundTransform = b[i].soundTransform;
				st.volume = 0;
				b[i].soundTransform = st;
			}
		}
		
		public function unmute()
		{
			trace("soundmgr unmute");
			var b:Array = buffer;
			for(var i:int = 0; i < b.length; i++)
			{
				var st:SoundTransform = b[i].soundTransform;
				st.volume = 1;
				b[i].soundTransform = st;
			}
		}
		
		public function sndStop(snd:Sound)
		{
			var idx:int = buffer_snd.indexOf(snd);
			if(idx > -1)
			{
				buffer[idx].stop();
				buffer.splice(idx, 1);
				buffer_snd.splice(idx, 1);
			}
		}
		
		private function sndPlay(snd:Sound, loops:int = 0)
		{
			if(muted) return;
			var sc:SoundChannel = snd.play(0, loops);
			if(sc != null)
			{
				sc.addEventListener(Event.SOUND_COMPLETE, sndComplete);
				buffer.push(sc);
				buffer_snd.push(snd);
			}
		}
		
		protected function sndComplete(event:Event):void
		{
			var sc:SoundChannel = event.target as SoundChannel;
			var idx:int = buffer.indexOf(sc);
			if(idx > -1)
			{
				buffer.splice(idx, 1);
				buffer_snd.splice(idx, 1);
			}
		}
		
	}
}