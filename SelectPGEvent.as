package
{
	import flash.events.Event;
	
	public class SelectPGEvent extends Event
	{
		public static const SELECT:String = "select";
		public var pgType:String;
		
		public function SelectPGEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}