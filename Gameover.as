package
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.filters.BlurFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	

	public class Gameover extends LadderView
	{
		public var btnMap:ResultButton;
		public var btnHome:ResultButton;
		public var bg:Bitmap;
		public var bgd:BitmapData;
		
		public function Gameover()
		{
			super();
			x = 512;
			y = 385;
		}
		public function setBackground(o:DisplayObject)
		{
			if(bgd == null)
			{
				bgd = new BitmapData(1024, 768, false);
				bg = new Bitmap(bgd);
				bg.x = -1024/2;
				bg.y = -768/2;
				addChildAt(bg, 0);
				//addChild(bg);
			}
			var m:Matrix = new Matrix();
			m.translate(o.x, o.y);
			bgd.draw(o, m);
			bgd.applyFilter(bgd, bgd.rect, new Point(), new BlurFilter(16, 16));
		}
	}
}