package
{
	import flash.display.MovieClip;
	
	public class MapBooleanStar extends MovieClip
	{
		private var _yellow:Boolean;
		public function MapBooleanStar()
		{
			super();
			stop();
		}

		public function get yellow():Boolean
		{
			return _yellow;
		}

		public function set yellow(value:Boolean):void
		{
			_yellow = value;
			gotoAndStop(_yellow ? "on" : "off");
		}

	}
}