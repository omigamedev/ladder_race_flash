package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class GameSea extends MovieClip
	{
		public function GameSea()
		{
			super();
			addEventListener(Event.ADDED, onAdded);
		}
		
		protected function onAdded(event:Event):void
		{
			removeEventListener(Event.ADDED, onAdded);
			initListeners();
			initWaves();
		}
		
		private function initWaves():void
		{
			for(var i:int = 0; i < 6; i++)
			{
				var w:GameWave = new GameWave();
				w.original_y = w.y = -i * 50;
				w.original_x = w.x = 0;//(Math.random()*2.0-1.0)*100;
				w.thetaSpeed = 0.03;
				w.theta = Math.random()*2.0*Math.PI;
				addChild(w);
			}
		}
		
		private function initListeners():void
		{
			addEventListener(Event.REMOVED, onRemove);
		}
		
		private function removeListeners():void
		{
			removeEventListener(Event.REMOVED, onRemove);
		}
		
		protected function onRemove(event:Event):void
		{
			removeListeners();
		}
	}
}