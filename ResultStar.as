package
{
	import flash.display.MovieClip;
	
	public class ResultStar extends MovieClip
	{
		private var _yellow:Boolean;
		public function ResultStar()
		{
			super();
			stop();
			yellow = false;
		}
		
		public function get yellow():Boolean
		{
			return _yellow;
		}
		
		public function set yellow(value:Boolean):void
		{
			_yellow = value;
			gotoAndStop(_yellow ? "yellow" : "grey");
		}
	}
}