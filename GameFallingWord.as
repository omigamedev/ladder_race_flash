package
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.GlowFilter;
	import flash.text.Font;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	public class GameFallingWord extends Sprite
	{
		public static var count:int = 0;
		public var txt:TextField;
		public var balloon:GameBalloon;
		public var vel:Number;
		
		public var cy:Number;
		public var cx:Number;
		private var theta:Number;
		private var colorsName:Array;
		private var colorsHex:Array;
		
		public function GameFallingWord(v:Number = 0.1)
		{
			super();
			
			theta = Math.random()*Math.PI*2;
			vel = v;
			
			colorsName = ["black", "brown", "grey", "pink", "purple", "red", "white", "yellow"];
			colorsHex = [0X0, 0X763A03, 0X505765, 0XEB63EC, 0X461858, 0XFF0000, 0XFFFFFF, 0XFFFF00];
			
			//var fonts:Array = Font.enumerateFonts(false);
			//for(var i:int = 0; i < fonts.length; i++) 
			//	trace(fonts[i].fontName);
			
			var r:uint = (Math.random() * 230) << 24;
			var g:uint = (Math.random() * 230) << 16;
			var b:uint = Math.random() * 230;
			var color:uint = r + g + b;
			
			balloon = new GameBalloon();
			balloon.y = -80;
			balloon.color = color;
			addChild(balloon);
			
			txt = new TextField();
			txt.embedFonts = true;
			txt.autoSize = TextFieldAutoSize.CENTER;
			txt.textColor = color;
			txt.defaultTextFormat = new TextFormat("Noteworthy Bold", 50);
			addChild(txt);
			
			mouseChildren = false;
			
			addEventListener(Event.ADDED, onAdded);
		}
		
		public function set text(s:String):void
		{
			txt.width = txt.height = 0;
			txt.text = s;
			txt.x = -txt.textWidth * 0.5;
			txt.y = -txt.textHeight * 0.5;
			//cy = txt.y;
			//cx = txt.x;
			var i:int;
			if((i = colorsName.indexOf(s)) != -1)
			{
				balloon.color = 0x0;//colorsHex[i];
				txt.textColor = 0x0;//colorsHex[i];
			}
		}
		
		public function get text():String
		{
			return txt.text;
		}
		
		protected function onAdded(event:Event):void
		{
			removeEventListener(Event.ADDED, onAdded);
			cy = y;
			cx = /*txt.*/x;
			txt.filters = [new GlowFilter(0xFFFFFF, 1.0, 12, 12, 10)];
			count++;
			initListeners();
		}
		
		public function initListeners():void
		{
			addEventListener(Event.REMOVED, onRemove);
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		protected function onEnterFrame(event:Event):void
		{
			y = cy// + Math.sin(theta*2) * 5;
			/*txt.*/x = cx + Math.cos(theta) * 10;
			/*txt.*/rotation = 5 * Math.cos(-theta);
			cy += vel;
			theta += 0.05;
			if(theta > Math.PI * 2) theta = 0;
		}
		
		public function removeListeners():void
		{
			removeEventListener(Event.REMOVED, onRemove);
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		protected function onRemove(event:Event):void
		{
			removeListeners();
			count--;
		}
		
		override public function toString():String
		{
			return text;
		}
	}
}