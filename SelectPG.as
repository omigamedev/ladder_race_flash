package
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;

	public class SelectPG extends LadderView
	{
		public var island:SelectIsland;
		public var pg1:CharacterBase;
		public var pg2:CharacterBase;
		public var selectedPG:CharacterBase;
		
		public var island_y:Number;
		public var pg1_y:Number;
		public var pg2_y:Number;
		public var theta:Number;
		
		public function SelectPG()
		{
			super();
			x = 512;
			y = 505;
			island_y = island.y;
			pg1_y = pg1.y;
			pg2_y = pg2.y;
			pg1.mouseChildren = false;
			pg2.mouseChildren = false;
			pg1.buttonMode = true;
			pg2.buttonMode = true;
			theta = 0;
			addEventListener(Event.ADDED, onAdded);
		}
		
		protected function onAdded(event:Event):void
		{
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
			addEventListener(Event.REMOVED, onRemove);
			pg1.addEventListener(MouseEvent.MOUSE_OVER, onPGOver);
			pg2.addEventListener(MouseEvent.MOUSE_OVER, onPGOver);
			pg1.addEventListener(MouseEvent.MOUSE_OUT, onPGOut);
			pg2.addEventListener(MouseEvent.MOUSE_OUT, onPGOut);
			pg1.addEventListener(MouseEvent.CLICK, onPGClick);
			pg2.addEventListener(MouseEvent.CLICK, onPGClick);
		}
		
		protected function onPGClick(event:MouseEvent):void
		{
			var pg:CharacterBase = event.target as CharacterBase;
			pg.happy();
			pg.addEventListener(CharacterBase.EVENT_HAPPY, onPGComplete);
			selectedPG = pg;
			mouseChildren = false;
		}
		
		protected function onPGComplete(event:Event):void
		{
			var pg:CharacterBase = event.target as CharacterBase;
			pg.removeEventListener(CharacterBase.EVENT_HAPPY, onPGComplete);
			var e:SelectPGEvent = new SelectPGEvent(SelectPGEvent.SELECT);
			e.pgType = pg.type;
			dispatchEvent(e);
		}
		
		protected function onPGOut(event:MouseEvent):void
		{
			var pg:CharacterBase = event.target as CharacterBase;
			pg.filters = null;
		}
		
		protected function onPGOver(event:MouseEvent):void
		{
			var pg:CharacterBase = event.target as CharacterBase;
			pg.filters = [new GlowFilter(0xFFFFFF)];
		}
		
		protected function onRemove(event:Event):void
		{
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			removeEventListener(Event.REMOVED, onRemove);
			pg1.removeEventListener(MouseEvent.MOUSE_OVER, onPGOver);
			pg2.removeEventListener(MouseEvent.MOUSE_OVER, onPGOver);
			pg1.removeEventListener(MouseEvent.MOUSE_OUT, onPGOut);
			pg2.removeEventListener(MouseEvent.MOUSE_OUT, onPGOut);
			pg1.removeEventListener(MouseEvent.CLICK, onPGClick);
			pg2.removeEventListener(MouseEvent.CLICK, onPGClick);
		}
		
		protected function onEnterFrame(event:Event):void
		{
			var off:Number = Math.cos(theta) * 10;
			island.y = island_y + off;
			pg1.y = pg1_y + off;
			pg2.y = pg2_y + off;
			theta += 0.1;
		}
	}
}