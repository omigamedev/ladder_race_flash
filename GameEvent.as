package
{
	import flash.events.Event;
	
	public class GameEvent extends Event
	{
		public static const COMPLETE:String = "complete";
		public var ncoins:int;
		public var score:int;
		public var nTotal:int;
		public var nWrong:int;
		public var nRight:int;
		public var listCorrect:String;
		public var listWrong:String;
		public var levelPassed:Boolean;
		public var wordsListString:String;
		
		public function GameEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}