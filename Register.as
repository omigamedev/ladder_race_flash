package
{
	import com.demonsters.debugger.MonsterDebugger;
	
	import fl.controls.Button;
	import fl.controls.ComboBox;
	import fl.controls.RadioButton;
	import fl.controls.RadioButtonGroup;
	import fl.data.DataProvider;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class Register extends LadderView
	{
		public static const EVENT_REGISTERED:String = "registered";
		
		public var cmbAge:ComboBox;
		public var cmbClass:ComboBox;
		public var cmbTongue:ComboBox;
		public var radioMale:RadioButton;
		public var radioFemale:RadioButton;
		public var btnOK:Button;
		public var txtError:TextField;
		
		public var radioGender:RadioButtonGroup;
		public var code:String;
		public var kid_id:int;
		
		public function Register()
		{
			super();
			x = 512;
			y = 505;
			
			var tf:TextFormat = new TextFormat("Noteworthy Bold", 23);
			var tfw:TextFormat = new TextFormat("Noteworthy Bold", 26, 0xFFFFFF);
			cmbAge.textField.setStyle("textFormat", tf);
			cmbAge.dropdown.setRendererStyle("textFormat", tf);
			cmbAge.dropdown.rowHeight = 40;
			
			cmbClass.textField.setStyle("textFormat", tf);
			cmbClass.dropdown.setRendererStyle("textFormat", tf);
			cmbClass.dropdown.rowHeight = 40;
			cmbTongue.textField.setStyle("textFormat", tf);
			cmbTongue.dropdown.setRendererStyle("textFormat", tf);
			cmbTongue.dropdown.rowHeight = 40;
			radioGender = new RadioButtonGroup("gender");
			radioMale.setStyle("textFormat", tfw);
			radioFemale.setStyle("textFormat", tfw);
			btnOK.setStyle("textFormat", tf);
			txtError.text = "";
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		protected function onAdded(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			
			var i:int;
			var dpAge:DataProvider = new DataProvider();
			for(i = 6; i <= 11; i++)
			{
				dpAge.addItem({label:i.toFixed(), id:i});
			}
			dpAge.addItem({label:"12+", id:i});
			cmbAge.dataProvider = dpAge;
			
			cmbClass.dataProvider = new DataProvider([
				{label:"Primary 1st class", id:1},
				{label:"Primary 2nd class", id:2},
				{label:"Primary 3rd class", id:3},
				{label:"Primary 4th class", id:4},
				{label:"Primary 5th class", id:5},
				{label:"Secondary 1st class", id:6},
				{label:"Secondary 2nd class", id:7},
			]);
			cmbClass.selectedIndex = 1;
			
			var dpTongue:DataProvider = new DataProvider();
			dpTongue.addItem({label:"Italiano", id:"ita"});
			dpTongue.addItem({label:"Español", id:"esp"});
			dpTongue.addItem({label:"Français", id:"fra"});
			dpTongue.addItem({label:"Deutsch", id:"deu"});
			dpTongue.addItem({label:"Nederlands", id:"nls"});
			cmbTongue.dataProvider = dpTongue;
			
			btnOK.addEventListener(MouseEvent.CLICK, onOKClick);
		}
		
		protected function onOKClick(event:MouseEvent):void
		{
			var ld:URLLoader = new URLLoader();
			ld.addEventListener(Event.COMPLETE, onRegisterResponse);
			ld.addEventListener(IOErrorEvent.IO_ERROR, onRegisterError);
			var base:String;
			
			if(root.loaderInfo.url.indexOf("file://") != -1) base = "http://www.colto.com/api/new/kid";
			else if(root.loaderInfo.url.indexOf("battleship") != -1)
			{
				dispatchEvent(new Event(EVENT_REGISTERED));
				return;
			}
			else base = "/api/new/kid";
			
			var url:URLRequest = new URLRequest(base + "?" + 
				"kid_malefemale="+(radioGender.selection.value)+
				"&kid_age="+cmbAge.selectedItem.id+
				"&kid_class="+cmbClass.selectedItem.id+
				"&kid_mother_tongue="+cmbTongue.selectedItem.id
			);
			
			MonsterDebugger.trace(this, url.url);
			ld.load(url);
			btnOK.enabled = false;
			txtError.text = "Loading..";
		}
		
		protected function onRegisterError(event:IOErrorEvent):void
		{
			txtError.text = "Connection error, please try again.";
			btnOK.enabled = true;
		}
		
		protected function onRegisterResponse(event:Event):void
		{
			var ld:URLLoader = event.target as URLLoader;
			MonsterDebugger.trace(this, ld.data);
			var jo:Object = JSON.parse(ld.data);
			if(jo.response == "OK")
			{
				code = jo.code;
				kid_id = jo.kid_id;
				dispatchEvent(new Event(EVENT_REGISTERED));
			}
			else if(jo.response == "ERROR")
			{
				txtError.text = "Registration error, please try again.";
			}
			else
			{
				txtError.text = "Error, please try again.";
			}
			btnOK.enabled = true;
		}
	}
}