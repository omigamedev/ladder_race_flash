package
{
	import com.demonsters.debugger.MonsterDebugger;
	import com.greensock.TweenLite;
	import com.greensock.easing.Linear;
	
	import fl.controls.Button;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	public class LadderRace extends MovieClip
	{
		public static var configTongue:String = "ita";
		public static var configClass:int = 2;
		public static var configGroup:int = Map.STARTER;
		public static var configKidID:int = 0;
		public static var configCode:String = "";
		
		public var fader:Sprite;
		public var current:LadderView;
		public var pgType:String;
		public var lang:String;
		public var clas:int;
		
		private var wordsLoader:URLLoader;
		private var wordsList:Array;
		private var totalPoints:int;
		private var unlockedLevel:int;
		private var currentLevel:int;
		private var registered:Boolean;
		private var levelStars:Array;
		private var startTime:Number;
		private var wordsData:String;
		
		public function LadderRace()
		{
			super();
			current = null;
			pgType = CharacterBase.PG_CAT;
			lang = "it";
			clas = 2;
			totalPoints = 0;
			currentLevel = 0;
			unlockedLevel = 0;
			levelStars = new Array(10);
			for(var i:int = 0; i < 10; i++) levelStars[i] = 0; 
			
			// Start the MonsterDebugger
			MonsterDebugger.initialize(this);
			MonsterDebugger.trace(this, "Hello World!");
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		protected function onAdded(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			
			if(root.loaderInfo.url.indexOf("file://") != -1)
			{
				stage.quality = StageQuality.MEDIUM;
				//SoundMgr.muted = true;
			}

			fader = new Sprite();
			fader.graphics.beginFill(0x0);
			fader.graphics.drawRect(0, 0, 1024, 768);
			fader.graphics.endFill();
			fader.cacheAsBitmap = true;
			fader.mouseEnabled = false;
			
			MonsterDebugger.trace(this, parent.root.loaderInfo.parameters);
			var code:String = parent.root.loaderInfo.parameters.play_code || "";
			if(code.length > 0)
			{
				var url:URLRequest;
				configCode = code;
				
				if(root.loaderInfo.url.indexOf("file://") != -1) 
					url = new URLRequest("http://www.colto.com/api/get/play/" + code);
				else if(root.loaderInfo.url.indexOf("battleship") != -1) return;
				else url = new URLRequest("/api/get/play/" + code);
				
				addChild(fader);
				
				var ld:URLLoader = new URLLoader();
				ld.addEventListener(Event.COMPLETE, onLoadingCodeLoaded);
				ld.addEventListener(IOErrorEvent.IO_ERROR, onLoadingCodeError);
				ld.load(url);
				MonsterDebugger.trace(this, "LOADING GAME " + url.url);
				trace("LOADING GAME " + url.url);
			}
			else
			{
				//gotoGame();
				gotoMainMenu();
			}
		}
		
		protected function onLoadingCodeError(event:IOErrorEvent):void
		{
			MonsterDebugger.trace(this, "ERROR LOADING GAME");
			trace("ERROR LOADING GAME");
		}
		
		protected function onLoadingCodeLoaded(event:Event):void
		{
			var ld:URLLoader = event.target as URLLoader;
			if(ld.data.length == 0)
			{
				MonsterDebugger.trace(this, "ERROR LOADING GAME EMPTY");
				return;
			}
			var j:Object = JSON.parse(ld.data);
			configCode = j.kid_history_code;
			configKidID = j.kid_history_kid_id;
			
			var jw:Object = JSON.parse(j.kid_history_words_set);
			wordsData = j.kid_history_words_set;
			wordsList = new Array();
			for(var o:Object in jw)
			{
				wordsList.push({en:jw[o].word_eng, tr:jw[o].word_mother_tongue, 
					wr1:jw[o].word_wrong_1, wr2:jw[o].word_wrong_2});
				if(jw[o].word_class == "Starters") configGroup = Map.STARTER;
				if(jw[o].word_class == "Movers") configGroup = Map.MOVER;
				if(jw[o].word_class == "Flyers") configGroup = Map.FLYER;
			}
			trace("words loaded " + jw.length);
			trace("group read is " + configGroup);
			registered = true;
			gotoMainMenu();
		}
		
		public function gotoMainMenu()
		{
			var mm:MainMenu = new MainMenu(registered);
			mm.btnPlay.addEventListener(MouseEvent.CLICK, onMainMenuButton);
			mm.btnEdit.addEventListener(MouseEvent.CLICK, onMainMenuButton);
			mm.btnRegister.addEventListener(MouseEvent.CLICK, onMainMenuButton);
			mm.code.btn.addEventListener(MouseEvent.CLICK, onMainMenuCodeEnter);
			goto(mm);
		}
		
		protected function onMainMenuCodeEnter(event:MouseEvent):void
		{
			var mm:MainMenu = event.target.parent.parent as MainMenu;
			
			var url:URLRequest;
			var code:String = mm.code.txt.text;
			if(code.length == 0)
			{
				mm.code.txtError.text = "Insert the code please.";
				return;
			}
			if(root.loaderInfo.url.indexOf("file://") != -1) 
				url = new URLRequest("http://www.colto.com/api/get/play/" + code);
			else if(root.loaderInfo.url.indexOf("battleship") != -1) return;
			else url = new URLRequest("/api/get/play/" + code);
			var ld:URLLoader = new URLLoader();
			ld.addEventListener(Event.COMPLETE, onMainMenuCodeLoaded);
			ld.addEventListener(IOErrorEvent.IO_ERROR, onMainMenuCodeError);
			ld.load(url);
			MonsterDebugger.trace(this, "LOADING GAME " + url.url);
			mm.code.txtError.text = "loading...";
		}
		
		protected function onMainMenuCodeError(event:IOErrorEvent):void
		{
			MonsterDebugger.trace(this, "ERROR LOADING GAME");
			(current as MainMenu).code.txtError.text = "Network error, please try again.";
		}
		
		protected function onMainMenuCodeLoaded(event:Event):void
		{
			var ld:URLLoader = event.target as URLLoader;
			if(ld.data.length == 0)
			{
				MonsterDebugger.trace(this, "ERROR LOADING GAME EMPTY");
				(current as MainMenu).code.txtError.text = "Wrong code, please try again.";
				return;
			}
			var j:Object = JSON.parse(ld.data);
			configCode = j.kid_history_code;
			configKidID = j.kid_history_kid_id;
			
			var jw:Object = JSON.parse(j.kid_history_words_set);
			wordsData = j.kid_history_words_set;
			wordsList = new Array();
			for(var o:Object in jw)
			{
				wordsList.push({en:jw[o].word_eng, tr:jw[o].word_mother_tongue, 
					wr1:jw[o].word_wrong_1, wr2:jw[o].word_wrong_2});
				if(jw[o].word_class == "Starters") configGroup = Map.STARTER;
				if(jw[o].word_class == "Movers") configGroup = Map.MOVER;
				if(jw[o].word_class == "Flyers") configGroup = Map.FLYER;
			}
			trace("words loaded " + jw.length);
			trace("group read is " + configGroup);
			(current as MainMenu).code.txtError.text = "Complete.";
			registered = true;
			gotoMainMenu();
		}
		
		private function onMainMenuButton(event:MouseEvent):void
		{
			var btn:MovieClip = event.target as MovieClip;
			var mm:MainMenu = btn.parent as MainMenu;
			mm.mouseEnabled = false;
			mm.mouseChildren = false;
			btn.removeEventListener(MouseEvent.CLICK, onMainMenuButton);
			if(btn == mm.btnPlay)
			{
				gotoSelectPG();
			}
			if(btn == mm.btnRegister || btn == mm.btnEdit)
			{
				gotoRegister();
			}
		}
		
		public function loadWords()
		{
			wordsLoader = new URLLoader();
			var r:URLRequest;
			MonsterDebugger.trace(this, root.loaderInfo.url);
			if(root.loaderInfo.url.indexOf("file://") != -1)
			{
				r = new URLRequest("http://www.colto.com/api/get/words/"+configTongue+"/"+configClass);
				//r = new URLRequest("words.html");
			}
			else if(root.loaderInfo.url.indexOf("battleship") != -1)
			{
				r = new URLRequest("words.html");
			}
			else
			{
				r = new URLRequest("/api/get/words/"+configTongue+"/"+configClass);
				//r = new URLRequest("words.html");
			}
			trace(r.url);
			MonsterDebugger.trace(this, r);
			wordsLoader.addEventListener(Event.COMPLETE, onWordsLoaded);
			wordsLoader.load(r);
		}
		
		protected function onWordsLoaded(event:Event):void
		{
			var j:Object = JSON.parse(wordsLoader.data);
			wordsData = wordsLoader.data;
			wordsList = new Array();
			for(var o:Object in j)
			{
				wordsList.push({en:j[o].word_eng, tr:j[o].word_mother_tongue, 
					wr1:j[o].word_wrong_1, wr2:j[o].word_wrong_2});
			}
			trace("words loaded " + j.length);
		}
		
		private function randomizeArray(array:Array):Array 
		{
			var newArray:Array = new Array();
			while(array.length > 0)
			{
				var obj:Array = array.splice(Math.floor(Math.random()*array.length), 1);
				newArray.push(obj[0]);
			}
			return newArray;
		}
		
		public function gotoRegister()
		{
			var reg:Register = new Register();
			reg.addEventListener(Register.EVENT_REGISTERED, onRegisterDone);
			goto(reg);
		}
		
		protected function onRegisterDone(event:Event):void
		{
			var reg:Register = event.target as Register;
			reg.removeEventListener(Register.EVENT_REGISTERED, onRegisterDone);
			reg.mouseEnabled = false;
			reg.mouseChildren = false;
			registered = true;
			configClass = reg.cmbClass.selectedItem.id;
			configTongue = reg.cmbTongue.selectedItem.id;
			configKidID = reg.kid_id;
			configCode = reg.code;
			if(configClass >= 1) configGroup = Map.STARTER;
			if(configClass >= 4) configGroup = Map.MOVER;
			if(configClass >= 6) configGroup = Map.FLYER;
			loadWords();
			gotoMainMenu();
		}
		
		protected function onRegisterOK(event:MouseEvent):void
		{
			var btn:Button = event.target as Button;
			var reg:Register = btn.parent as Register;
			btn.removeEventListener(MouseEvent.CLICK, onRegisterOK);
			reg.mouseEnabled = false;
			reg.mouseChildren = false;
			gotoSelectPG();
		}
		
		public function gotoSelectPG()
		{
			var sel:SelectPG = new SelectPG();
			sel.addEventListener(SelectPGEvent.SELECT, onPGSelect);
			goto(sel);
		}
		
		protected function onPGSelect(event:SelectPGEvent):void
		{
			var sel:SelectPG = event.target as SelectPG;
			sel.removeEventListener(SelectPGEvent.SELECT, onPGSelect);
			sel.mouseEnabled = false;
			sel.mouseChildren = false;
			pgType = event.pgType;
			gotoMap();
			
			var url:URLRequest;
			var name:String;
			if(pgType == CharacterBase.PG_CAT) name = "cat";
			if(pgType == CharacterBase.PG_HAMSTER) name = "hamster";
			if(root.loaderInfo.url.indexOf("file://") != -1) 
				url = new URLRequest("http://www.colto.com/api/save/character/" + name);
			else if(root.loaderInfo.url.indexOf("battleship") != -1) return;
			else url = new URLRequest("/api/save/character/" + name);
			var ld:URLLoader = new URLLoader();
			ld.load(url);
			trace(url.url);
		}
		
		public function gotoMap()
		{
			var map:Map = new Map(pgType, totalPoints, unlockedLevel, levelStars, configGroup);
			map.lv1.addEventListener(MouseEvent.CLICK, onMapLevelClick);
			map.lv2.addEventListener(MouseEvent.CLICK, onMapLevelClick);
			map.lv3.addEventListener(MouseEvent.CLICK, onMapLevelClick);
			map.lv4.addEventListener(MouseEvent.CLICK, onMapLevelClick);
			map.btnBack.addEventListener(MouseEvent.CLICK, onMapBackClick);
			goto(map);
		}
		
		protected function onMapBackClick(event:MouseEvent):void
		{
			var btn:MovieClip = event.target as MovieClip;
			var map:Map = btn.parent as Map;
			map.mouseEnabled = false;
			map.mouseChildren = false;
			btn.removeEventListener(MouseEvent.CLICK, onMapBackClick);
			gotoMainMenu();
		}
		
		protected function onMapLevelClick(event:MouseEvent):void
		{
			var lv:MapLevel = event.target as MapLevel;
			if(lv.locked) return;
			
			var map:Map = lv.parent as Map;
			map.lv1.removeEventListener(MouseEvent.CLICK, onMapLevelClick);
			map.lv2.removeEventListener(MouseEvent.CLICK, onMapLevelClick);
			map.lv3.removeEventListener(MouseEvent.CLICK, onMapLevelClick);
			map.lv4.removeEventListener(MouseEvent.CLICK, onMapLevelClick);
			map.mouseEnabled = false;
			map.mouseChildren = false;
			if(lv == map.lv1) currentLevel = 0;
			if(lv == map.lv2) currentLevel = 1;
			if(lv == map.lv3) currentLevel = 2;
			if(lv == map.lv4) currentLevel = 3;
			gotoGame();
		}
		
		public function gotoGame()
		{
			var index:int = currentLevel * 6;
			var game:Game = new Game(pgType, randomizeArray(wordsList.slice(index, index+6)), currentLevel);
			game.addEventListener(GameEvent.COMPLETE, onGameComplete);
			game.mpause.btnMap.addEventListener(MouseEvent.CLICK, onGameButton);
			game.mpause.btnReplay.addEventListener(MouseEvent.CLICK, onGameButton);
			goto(game);
			startTime = (new Date).time;
		}
		
		protected function onGameComplete(event:GameEvent):void
		{
			var game:Game = event.target as Game;
			game.removeEventListener(GameEvent.COMPLETE, onGameComplete);
			game.mouseEnabled = false;
			game.mouseChildren = false;
			SoundMgr.fadeOut(3);
			totalPoints += event.score;
			var maxLevel:int = 0;
			if(configGroup == Map.STARTER) maxLevel = 4;
			if(configGroup == Map.MOVER) maxLevel = 5;
			if(configGroup == Map.FLYER) maxLevel = 6;
			if(event.levelPassed)
			{
				unlockedLevel = Math.min(unlockedLevel+1, maxLevel-1);
				levelStars[currentLevel] = Math.max(coins2star(event.ncoins), levelStars[currentLevel]);
				if(unlockedLevel == currentLevel) gotoGameover(game, event);
				else gotoResult(game, event);
			}
			else
			{
				gotoResult(game, event);
			}
			
			var dat:URLVariables = new URLVariables();
			dat.play_code = configCode;
			dat.play_date = startTime;
			dat.words_set = wordsData;
			dat.kid_id = configKidID;
			dat.completion_time = (new Date).time;
			dat.words_total = event.nTotal;
			dat.words_right = event.nRight;
			dat.words_wrong = event.nWrong;
			dat.score = totalPoints;
			
			trace("SAVING GAME: " + dat.toString());
			MonsterDebugger.trace(this, "SAVING GAME: " + dat.toString());
			var url:URLRequest = new URLRequest("http://www.colto.com/api/save/play");
			
			if(root.loaderInfo.url.indexOf("file://") != -1) url = new URLRequest("http://www.colto.com/api/save/play");
			else if(root.loaderInfo.url.indexOf("battleship") != -1) return;
			else url = new URLRequest("/api/save/play");
			
			url.data = dat;
			url.method = URLRequestMethod.POST;
			var ld:URLLoader = new URLLoader();
			ld.addEventListener(Event.COMPLETE, onGameSaveComplete);
			ld.addEventListener(IOErrorEvent.IO_ERROR, onGameSaveError);
			ld.load(url);
			trace(url.url);
			MonsterDebugger.trace(this, url.url);
		}
		
		protected function onGameSaveError(event:IOErrorEvent):void
		{
			trace("ERROR SAVING " + event);
			MonsterDebugger.trace(this, "ERROR SAVING " + event);
		}
		
		protected function onGameSaveComplete(event:Event):void
		{
			var ld:URLLoader = event.target as URLLoader;
			var jo:Object = JSON.parse(ld.data);
			if(jo.response == "OK")
			{
				MonsterDebugger.trace(this, "GAME SAVED");
			}
			else if(jo.response == "ERROR")
			{
				MonsterDebugger.trace(this, "ERROR SAVING");
			}
			else
			{
				MonsterDebugger.trace(this, "ERROR SAVING UNKNOWN");
			}
			MonsterDebugger.trace(this, ld.data);
		}
		
		protected function onGameButton(event:MouseEvent):void
		{
			var btn:ResultButton = event.target as ResultButton;
			var game:Game = btn.parent.parent as Game;
			game.mpause.btnMap.removeEventListener(MouseEvent.CLICK, onGameButton);
			game.mpause.btnReplay.removeEventListener(MouseEvent.CLICK, onGameButton);
			game.mouseEnabled = false;
			game.mouseChildren = false;
			if(btn == game.mpause.btnMap)
			{
				gotoMap();
			}
			else if(btn == game.mpause.btnReplay)
			{
				gotoGame();
			}
			SoundMgr.fadeOut(1);
		}
		
		public function gotoResult(blurred:DisplayObject, event:GameEvent)
		{
			var result:Result = new Result(coins2star(event.ncoins), event.listCorrect, 
				event.listWrong, event.score, event.levelPassed);
			result.setBackground(blurred);
			result.btnMap.addEventListener(MouseEvent.CLICK, onResultButton);
			result.btnReplay.addEventListener(MouseEvent.CLICK, onResultButton);
			result.btnNext.addEventListener(MouseEvent.CLICK, onResultButton);
			goto(result);
		}
		
		private function coins2star(c:int):int
		{
			if(c >= 6) return 3;
			if(c >= 4) return 2;
			if(c >= 2) return 1;
			return 0;
		}
		
		protected function onResultButton(event:MouseEvent):void
		{
			var btn:ResultButton = event.target as ResultButton;
			var result:Result = btn.parent as Result;
			result.btnMap.removeEventListener(MouseEvent.CLICK, onResultButton);
			result.btnReplay.removeEventListener(MouseEvent.CLICK, onResultButton);
			result.btnNext.removeEventListener(MouseEvent.CLICK, onResultButton);
			result.mouseEnabled = false;
			result.mouseChildren = false;
			if(btn == result.btnMap)
			{
				gotoMap();
			}
			else if(btn == result.btnNext)
			{
				currentLevel++;
				if(currentLevel > unlockedLevel) currentLevel = unlockedLevel;
				gotoGame();
			}
			else if(btn == result.btnReplay)
			{
				gotoGame();
			}
		}
		
		public function gotoGameover(blurred:DisplayObject, event:GameEvent)
		{
			var gameover:Gameover = new Gameover();
			gameover.setBackground(blurred);
			gameover.btnMap.addEventListener(MouseEvent.CLICK, onGameoverMap);
			gameover.btnHome.addEventListener(MouseEvent.CLICK, onGameoverHome);
			goto(gameover);
		}
		
		protected function onGameoverHome(event:MouseEvent):void
		{
			gotoMainMenu();
		}
		
		protected function onGameoverMap(event:MouseEvent):void
		{
			gotoMap();
		}
		
		public function goto(next:LadderView)
		{
			if(current != null)
			{
				var tm:Number = 0;
				if(next is Result) tm = 2.5;
				fader.alpha = 0;
				addChild(fader);
				TweenLite.to(fader, 0.5+tm, {alpha:1, ease:Linear.easeIn, onCompleteParams:[this, current, next], 
					onComplete:function(t:MovieClip, v:LadderView, n:LadderView)
					{
						t.removeChild(v);
						if(n) t.addChildAt(n, 0);
						t.stage.focus = n;
						TweenLite.to(fader, 0.5, {alpha:0, ease:Linear.easeIn, onCompleteParams:[t, n], 
							onComplete:function(t:MovieClip, n:LadderView)
							{
								t.removeChild(fader);
								if(n) n.start();
							}
						});
					}
				});
			}
			else
			{
				if(next) addChild(next);
				fader.alpha = 1;
				addChild(fader);
				TweenLite.to(fader, 2, {alpha:0, ease:Linear.easeIn, onCompleteParams:[this, next], 
					onComplete:function(t:MovieClip, n:LadderView)
					{
						t.removeChild(fader);
						if(n) n.start();
					}
				});
			}
			current = next;
		}
	}
}